﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo3
{
    class Program
    {
        static void Main(string[] args)
        {
            #region 九九乘法口诀表
            Console.WriteLine("九九乘法口诀表");
            for (int i = 1; i < 10; i++)
            {
                for (int j = 1; j <= i; j++)
                {
                    Console.Write(i + "x" + j + "=" + i * j + "\t");
                }
                Console.WriteLine();
            }
            #endregion
            //打印直角三角形
            Console.WriteLine("请输入您想要的等腰直角三角形的列数 ");
            int n = Convert.ToInt32 ( Console.ReadLine());
           for( int a = 1; a <= n ; a++)
            {
                for( int b = 1 ; b <= a ; b++ )
                {
                    Console.Write("*");
                }
                Console.Write("\n");

            }
            Console.ReadLine();
        }
    }
}